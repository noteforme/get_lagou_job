#!/urs/bin/env python
# _*_ coding:utf-8 _*_


import requests,time,datetime
from lxml import etree

now = datetime.datetime.now()
timeStamp = int(now.timestamp() * 1000)
geshi = "%Y%m%d%H%M%S"
time1 = datetime.datetime.strftime(now, geshi)

headers = {
    'Accept': 'application/json, text/javascript, */*; q=0.01',
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'zh-CN,zh;q=0.9,en;q=0.8,zh-TW;q=0.7,ja;q=0.6,und;q=0.5,da;q=0.4,mt;q=0.3',
    'Connection': 'keep-alive',
    'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8',
    'Cookie': 'JSESSIONID=ABAAABAAAGFABEF07AF0B3FB239326C00819AB47CC27121; _ga=GA1.2.1790009235.1550040231; user_trace_token=20190213144351-b926c680-2f5a-11e9-b62a-525400f775ce; LGUID=20190213144351-b926c931-2f5a-11e9-b62a-525400f775ce; _gid=GA1.2.190371599.1550645660; Hm_lvt_4233e74dff0ae5bd0a3d81c6ccf756e6=1550040231,1550040236,1550645661; LGSID=20190220145420-59622be7-34dc-11e9-8282-5254005c3644; PRE_UTM=; PRE_HOST=cn.bing.com; PRE_SITE=https%3A%2F%2Fcn.bing.com%2F; PRE_LAND=https%3A%2F%2Fwww.lagou.com%2F; index_location_city=%E6%88%90%E9%83%BD; Hm_lpvt_4233e74dff0ae5bd0a3d81c6ccf756e6={timeStamp}; LGRID={time}-5cf6a7d4-34dc-11e9-97f8-525400f775ce; TG-TRACK-CODE=index_search; SEARCH_ID=349cef37a45942beb37dc47b38f05907'.format(
        timeStamp=timeStamp, time=time1),
    'Host': 'www.lagou.com',
    'Origin': 'https://www.lagou.com',
    'Referer': 'https://www.lagou.com/jobs/list_%E6%B5%8B%E8%AF%95?labelWords=&fromSearch=true&suginput=',
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36',
    'X-Anit-Forge-Code': '0',
    'X-Anit-Forge-Token': 'None',
    'X-Requested-With': 'XMLHttpRequest'
}

#post body
data = {
    'first':False,
    'pn':1,
    'kd':'测试'
}
#post请求页面，返回job列表
def get_job(data):

    url = 'https://www.lagou.com/jobs/positionAjax.json?city=%E6%88%90%E9%83%BD&needAddtionalResult=false&isSchoolJob=0'
    # page = requests.post(url,headers=headers,data=data,cookies=cookie)
    page = requests.post(url,headers=headers,data=data)
    page.encoding = 'utf8'
    result = page.json()
    # print(result.get('status'))
    # print(page.text)

    if result.get('status') == False:
        print(page.text)
        return
    jobs = result.get('content').get('positionResult').get('result')
    #ip受限制时第一个get不会报错，但是第二个get会报错，因为第一个get返回的是None，不是字典就不能再用get方法
    # print(jobs)
    for job in jobs:
        companyShortName = job.get('companyShortName')
        positionId = job['positionId']  # 主页ID
        companyFullName = job['companyFullName']  # 公司全名
        companyLabelList = job['companyLabelList']  # 福利待遇
        companySize = job['companySize']  # 公司规模
        industryField = job['industryField']
        createTime = job['createTime']  # 发布时间
        district = job['district']  # 地区
        education = job['education']  # 学历要求
        financeStage = job['financeStage']  # 上市否
        firstType = job['firstType']  # 类型
        secondType = job['secondType']  # 类型
        formatCreateTime = job['formatCreateTime']  # 发布时间
        publisherId = job['publisherId']  # 发布人ID
        salary = job['salary']  # 薪资 #'10k-18k'
        salary_low = salary.split('-')[0]  #获取工资范围中的第一个值
        salary_high = salary.split('-')[1] #获取工资范围中的第二个值

        workYear = job['workYear']  # 工作年限
        positionName = job['positionName']  #职位名称
        jobNature = job['jobNature']  # 全职
        positionAdvantage = job['positionAdvantage']  # 工作福利
        positionLables = job['positionLables']  # 工种
        # print(companyShortName,'\n')
        # #提取job详情
        detail_url = 'https://www.lagou.com/jobs/{}.html'.format(positionId)
        # print(positionAdvantage)
        positionAdvantage = positionAdvantage.replace(',','-')
        positionAdvantage = positionAdvantage.replace('\n','')
        # print(positionAdvantage)
        # exit()
        with open('lagoujob201902201625.csv','a',encoding='gbk') as fb:
            temp = f'{detail_url},{positionId},{companyShortName},{positionName},{salary_low},{salary_high},{workYear},{createTime},{formatCreateTime},{district},{education},{companySize},{positionAdvantage}\n'
            print(temp)
            fb.write(temp)


        # #提取job详情
        # detail_url = 'https://www.lagou.com/jobs/{}.html'.format(positionId)
        # print(detail_url,'\n')
        #
        # # response = requests.get(detail_url,headers=headers,cookies=cookies)
        # response = requests.get(detail_url,cookies=cookies,headers=headers)
        # response.encoding = 'utf8'
        # # print(response.text,'\n')
        # # exit()
        # tree = etree.HTML(response.text) #初始化网页为etree样式
        # #通过etree xpath提取job desc
        # desc = tree.xpath("//dl[@class='job_detail']/dd[@class='job_bt']/div/p/text()")
        # print('{} job link：{}'.format(companyShortName,detail_url))
        # # print(positionLables)
        # x = ''
        # for label in positionLables:
        #     x += label + ','
        # print('技能：{}'.format(x))
        # # print('描述：{}\n'.format(desc))
        # for des in desc:
        #     print(des)

def url(data):
    try:
        for pn in range(31):
            data['pn'] = pn  #更新data字典的pn值为x
            try:
                time.sleep(5)
                get_job(data)
                pageNum = pn + 1
                print(f'当前已完成请求{pageNum}页的数据')
            except Exception as e:
                print(e)

    except Exception as e:
        print(e)

if __name__ == '__main__':
    url(data)